package com.epam.controller;

import com.epam.model.Game;
import com.epam.model.Square;

public class MainController {
    private StringBuilder data;

    public MainController(int m, int n, int probability) {
        data = loadDataFromModel(m, n, probability);
    }

    private StringBuilder loadDataFromModel(int m, int n, int probability) {
        StringBuilder data = new StringBuilder();
        Game game = new Game(m, n, probability);
        Square[][] squares = game.getField();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                data.append(squares[i][j].isBomb()).append(" ");
            }
            data.append("\n");
        }
        data.append("\n");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                data.append(squares[i][j].getCountNeightboursBomb()).append(" ");
            }
            data.append("\n");
        }
        return data;
    }

    public StringBuilder getData() {
        return data;
    }

}
