package com.epam.model;

import java.util.Random;

public class Game {

    private int m;
    private int n;
    private int probability;
    private Square[][] field;

    public Game(int m, int n, int probability) {
        this.m = m;
        this.n = n;
        this.probability = probability;
        this.field = countNeightbourBombsNumber(m, n, probability);
    }

    public Square[][] getField() {
        return field;
    }

    private Square[][] createFild(int m, int n, int probability) {
        Square[][] field = new Square[m][n];
        Square square;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                square = new Square(randomBoleanWithProbabilityTrue(probability));
                field[i][j] = square;
            }
        }
        return field;
    }

    private boolean randomBoleanWithProbabilityTrue(int probability) {
        Random random = new Random();
        int numb = random.nextInt(100);
        return numb <= probability;
    }

    private Square[][] countNeightbourBombsNumber(int m, int n, int probability) {
        int countBombs = 0;
        Square[][] squares = createFild(m, n, probability);
        for (int i = 0; i < squares.length; i++) {
            for (int j = 0; j < squares[i].length; j++) {
                if (!squares[i][j].isBomb()) {
                    if (i != squares.length - 1) {
                        if (j != 0) {
                            if (squares[i][j - 1].isBomb()) {
                                countBombs++;
                            }
                            if (squares[i + 1][j - 1].isBomb()) {
                                countBombs++;
                            }
                        }
                        if (squares[i + 1][j].isBomb()) {
                            countBombs++;
                        }
                        if (j != squares[i].length - 1) {

                            if (squares[i][j + 1].isBomb()) {
                                countBombs++;
                            }
                            if (squares[i + 1][j + 1].isBomb()) {
                                countBombs++;
                            }
                        }
                    }
                    if (i != 0) {
                        if (j != 0) {
                            if (squares[i][j - 1].isBomb() && i == squares.length - 1) {
                                countBombs++;
                            }
                            if (squares[i - 1][j - 1].isBomb()) {
                                countBombs++;
                            }
                        }
                        if (squares[i - 1][j].isBomb()) {
                            countBombs++;
                        }
                        if (j != squares[i].length - 1) {
                            if (squares[i][j + 1].isBomb() && i == squares.length - 1) {
                                countBombs++;
                            }
                            if (squares[i - 1][j + 1].isBomb()) {
                                countBombs++;
                            }
                        }
                    }
                }
                squares[i][j].setCountNeightboursBomb(countBombs);
                countBombs = 0;
            }
        }
        return squares;
    }
}