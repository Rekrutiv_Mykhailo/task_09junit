package com.epam.model;

public class Square {

    private boolean bomb;
    private int countNeightboursBomb;

    public Square() {
    }

    public Square(boolean bomb, int countNeightboursBomb) {
        this.bomb = bomb;
        this.countNeightboursBomb = countNeightboursBomb;
    }


     Square(boolean bomb) {
        this.bomb = bomb;
    }

    public boolean isBomb() {
        return bomb;
    }

    public void setBomb(boolean bomb) {
        this.bomb = bomb;
    }

    public int getCountNeightboursBomb() {
        return countNeightboursBomb;
    }

    public void setCountNeightboursBomb(int countNeightboursBomb) {
        this.countNeightboursBomb = countNeightboursBomb;
    }
}
