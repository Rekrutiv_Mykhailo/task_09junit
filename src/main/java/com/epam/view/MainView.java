package com.epam.view;

import com.epam.controller.MainController;
import com.epam.service.CheckingCorrectMenuException;

public class MainView extends AbstractView {

    public MainView() {
        loadData();
    }

    @Override
    public void output(String mess) {
        super.output(mess);
    }

    @Override
    public int checkingInputInt() {
        return super.checkingInputInt();
    }

    @Override
    public int checkingInputProbability() throws CheckingCorrectMenuException {
        return super.checkingInputProbability();
    }

    @Override
    public int[] checkingInputSizeGameField() throws CheckingCorrectMenuException {
        return super.checkingInputSizeGameField();
    }

    @Override
    public Integer inputProbability() {
        return super.inputProbability();
    }

    @Override
    public int[] inputSizeField() {
        return super.inputSizeField();
    }

    private void loadData() {
        int[] sizeField = inputSizeField();
        int probability = inputProbability();
        StringBuilder stringBuilder = new MainController(sizeField[0], sizeField[1], probability).getData();
        output(stringBuilder.toString());
    }
}
