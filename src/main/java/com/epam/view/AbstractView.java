package com.epam.view;

import com.epam.service.CheckingCorrectMenuException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.InputMismatchException;
import java.util.Scanner;

public class AbstractView {

    private Logger logger = LogManager.getLogger(AbstractView.class);

    public void output(String mess) {
        logger.info(mess);
    }

    public int checkingInputInt() {
        int number = 0;
        Scanner in = new Scanner(System.in);
        try {
            number = in.nextInt();
        } catch (InputMismatchException e) {
            logger.error("incorrect iput");
            checkingInputInt();
        }
        return number;
    }

    public int checkingInputProbability() throws CheckingCorrectMenuException {
        output("Please enter probability");
        int number = checkingInputInt();
        if (number >= 0 && number <= 100) {
            return number;
        } else {
            throw new CheckingCorrectMenuException("Please enter probability from 0 to 100");
        }
    }

    public int[] checkingInputSizeGameField() throws CheckingCorrectMenuException {
        int[] size = new int[2];
        output("Please enter first size field");
        int m = checkingInputInt();
        output("Please enter second size field");
        int n = checkingInputInt();
        if ((n >= 2) && (m >= 2) && (m > 2 || n > 2)) {
//        if(true){
            size[0] = m;
            size[1] = n;
            return size;
        } else {
            throw new CheckingCorrectMenuException("Please enter size at least 3x2 or 2x3");
        }
    }

    public Integer inputProbability() {
        int numb;
        try {
            numb = checkingInputProbability();
        } catch (CheckingCorrectMenuException e) {
            logger.error(e);
            numb = inputProbability();
        }
        return numb;
    }

    public int[] inputSizeField() {
        int[] size;
        try {
            size = checkingInputSizeGameField();
            return size;
        } catch (CheckingCorrectMenuException e) {
            logger.error(e);
            size = inputSizeField();
        }
        return size;
    }
}
